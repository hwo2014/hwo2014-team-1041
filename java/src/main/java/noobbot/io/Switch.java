package noobbot.io;

public class Switch extends SendMsg {
    private String whichWay;

    public Switch(String whichWay) {
        this.whichWay = whichWay;
    }

    @Override
    protected Object msgData() {
        return whichWay;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}