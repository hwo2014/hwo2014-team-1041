package noobbot.model;

import noobbot.model.TrackPiece;

/**
 * Created by kristjanveskimae on 18/04/2014.
 */
public class TrackStraightPiece extends TrackPiece {
    private final double length;
    public TrackStraightPiece(double initLength, boolean initSwitch) {
        super(initSwitch);
        this.length = initLength;
    }

    @Override
    public String getInfo() {
        return "length=" + length;
    }

    @Override
    public double calculateLength() {
        return length;
    }

}
