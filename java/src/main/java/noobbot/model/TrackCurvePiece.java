package noobbot.model;

public class TrackCurvePiece extends TrackPiece {
    private final double radius;
    private final double angle;

    public TrackCurvePiece(double initRadius, double initAngle, boolean initSwitch) {
        super(initSwitch);
        this.radius = initRadius;
        this.angle = initAngle;
    }

    @Override
    public String getInfo() {
        return "radius=" + radius + ", angle=" + angle;
    }

    @Override
    public boolean isCurve() {
        return true;
    }

    @Override
    public double calculateLength() {
        return radius * Math.PI * Math.abs(angle) / 180;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    public double getCorrectAngle(double distanceCovered) {
        return angle * distanceCovered / radius;
    }

}
