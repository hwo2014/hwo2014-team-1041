package noobbot.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Track {

    private final List<TrackPiece> pieces = new ArrayList<>();

    public void handleGameInit(Object data) {
        Map dataMap = (Map) data;
        Map raceMap = (Map) dataMap.get("race");
        Map trackMap = (Map) raceMap.get("track");
        List piecesList = (List) trackMap.get("pieces");
        for (Object o : piecesList) {
            Map pieceMap = (Map) o;
            boolean isSwitch = false;
            if (pieceMap.containsKey("switch")) {
                isSwitch = (boolean) pieceMap.get("switch");
            }
            if (pieceMap.containsKey("length")) {
                double length = (double) pieceMap.get("length");
                addPiece(new TrackStraightPiece(length, isSwitch));
            } else {
                double radius = (double) pieceMap.get("radius");
                double angle = (double) pieceMap.get("angle");
                addPiece(new TrackCurvePiece(radius, angle, isSwitch));
            }
        }
    }

    public void addPiece(TrackPiece piece) {
        pieces.add(piece);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append("[");
        double start = 0, end = 0;
        int pieceId = 0;
        int section = 1;
        for (TrackPiece piece : pieces) {
            if (pieces.indexOf(piece) == 0 || piece.isSwitch()) {
                sb.append("===== SECTION "+section+" =====\n");
                section++;
            }
            end += piece.calculateLength();
            sb.append("id=" + pieceId + "; ");
            sb.append(piece);
            sb.append("; start=" + start + ", end=" + end);
            sb.append("\n");
            start = end;
            pieceId++;
        }
        sb.append("]");
        return sb.toString();
    }

    public List<TrackPiece> getPieces() {
        return pieces;
    }
}
