package noobbot.model;

public abstract class TrackPiece {
    private boolean isSwitch = false;

    public TrackPiece(boolean initSwitch) {
        isSwitch = initSwitch;
    }

    @Override
    public String toString() {
        String ret = getClass().getSimpleName() + "[" + getInfo() + ", length=" + calculateLength() + ", switch=" + isSwitch + "]";
        return ret;
    }

    public abstract String getInfo();

    public boolean isCurve() {
        return false;
    }

    public abstract double calculateLength();

    public boolean isSwitch() {
        return isSwitch;
    }


}
