package noobbot.simulation;

import noobbot.model.Track;
import noobbot.model.TrackCurvePiece;
import noobbot.model.TrackPiece;

import java.util.*;

/**
 * Created by kristjanveskimae on 18/04/2014.
 */
public class Simulation {

    private List<SimulationPiece> pieces = new ArrayList<>();

    private static List<Integer> middleCurvePieces = new ArrayList<>(Arrays.asList(
            5,
            // 6,
            15,
            // 16,
            17,
            19,
            20,
            21,
            22,
            27,
            32,
            33))
    , innerPieces = new ArrayList<>(Arrays.asList(29, 30));

    public void addPiece(SimulationPiece piece) {
        pieces.add(piece);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getSimpleName());
        sb.append('[');
        for (SimulationPiece piece : pieces) {
            sb.append(piece);
            sb.append("\n");
        }
        sb.append(']');
        return sb.toString();
    }

    public static Simulation createSimulation(Track track) {
        List<TrackPiece> trackPieces = track.getPieces();
        Simulation ret = new Simulation();
        double position = 0;
        double shiftForNextPiece = 0;
        Iterator<TrackPiece> iterator = trackPieces.iterator();
        while (iterator.hasNext()) {
            TrackPiece piece = iterator.next();
            int pieceIdx = trackPieces.indexOf(piece);
            double pieceEnd = position + piece.calculateLength() - shiftForNextPiece;
            int pieceCount = (int) Math.ceil((piece.calculateLength() - shiftForNextPiece) / Constants.SIMULATION_PIECE_LENGTH);
            shiftForNextPiece = 0;
            for (int i = 0; i < pieceCount; i++) {
                double start = position;
                double end = position + Constants.SIMULATION_PIECE_LENGTH;
                boolean curve = piece.isCurve();
                double radius = -1, correctAngle = -1;
                SimulationPiece.SimulationPieceType type = SimulationPiece.SimulationPieceType.NONE;
                if (curve) {
                    radius = ((TrackCurvePiece) piece).getRadius();
                    correctAngle = ((TrackCurvePiece)piece).getCorrectAngle(start + Constants.SIMULATION_PIECE_LENGTH / 2.0 - (pieceEnd - piece.calculateLength()));
                    if (innerPieces.contains(pieceIdx)) {
                        // inner curve
                        radius -= Constants.LANE_WIDTH;
                    }
                    if (middleCurvePieces.contains(pieceIdx)) {
                        type = SimulationPiece.SimulationPieceType.CURVE_MIDDLE;
                    }
                }
                SimulationPiece simulationPiece = new SimulationPiece(start, end, curve, radius, type, correctAngle);
                ret.addPiece(simulationPiece);
                position = end;
            }
        }
        return ret;
    }

    public List<SimulationPiece> getPieces() {
        return pieces;
    }
}
