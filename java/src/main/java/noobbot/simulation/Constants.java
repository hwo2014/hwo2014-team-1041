package noobbot.simulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kristjanveskimae on 18/04/2014.
 */
public interface Constants {
    double SIMULATION_PIECE_LENGTH = 5.0,
    NORMAL_FORCE =
            0.56
            ,
    SMALL_NORMAL_FORCE =
            0.52
            ,
    MAX_SPEED = 10,
    MAX_ACCELERATION = 0.17,
    THROTTLE_FROM_ACCELERATION_MULTIPLIER = 5.0,
    THROTTLE_FROM_ACCELERATION_ADD = 0.1,
    LANE_WIDTH = 20.0;
}
