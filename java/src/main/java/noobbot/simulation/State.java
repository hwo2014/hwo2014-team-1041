package noobbot.simulation;

import noobbot.model.Track;
import java.util.List;
import java.util.Map;

public class State {

    private final Track track;

    private int oldPieceIdx = 0, currentPieceIndex = -1, currentSimulationPiece = 0;
    private double oldInPieceDistance = 0.0, currentInPieceDistance = -1, lastCorrectSpeed = 0, position = 0.0, speed = 0.0, angle = 0.0, angleSpeed = 0.0;
    private boolean pieceChanged = false;

    public State(Track track) {
        this.track = track;
    }

    /*if (state.oldPieceIdx == newPieceIndex) {
                    distanceCoveredDuringTick = newInPieceDistance - state.oldInPieceDistance;
                } else {
                    distanceCoveredDuringTick += newInPieceDistance;
                    distanceCoveredDuringTick += track.getPieceLength(state.oldPieceIdx) - state.oldInPieceDistance;
                    state.oldPieceIdx = newPieceIndex;
                    if (newPieceIndex == 12
                        // || newInPieceDistance == 28
                            ) {
                        state.sendSwitch = "Right";
                    } else if (newPieceIndex == 17
                        // || newInPieceDistance == 34
                            ) {
                        state.sendSwitch = "Left";
                    }
                }*/
    public void update(Object data) {
        List dataAsList = (List) data;
        for (Object o : dataAsList) {
            Map valueMap = (Map) o;
            Map idMap = (Map) valueMap.get("id");
            String name = (String) idMap.get("name");
            if (name.equals("Dragons")) {
                Map positionAsMap = (Map) valueMap.get("piecePosition");
                currentPieceIndex = ((Double) positionAsMap.get("pieceIndex")).intValue();
                currentInPieceDistance = (double) positionAsMap.get("inPieceDistance");
                if (oldPieceIdx == currentPieceIndex) {
                    speed = currentInPieceDistance - oldInPieceDistance;
                } else {
                    speed += currentInPieceDistance;
                    speed += track.getPieces().get(oldPieceIdx).calculateLength() - oldInPieceDistance;
                }
                double newAngle = (double) valueMap.get("angle");
                angleSpeed = Math.abs(newAngle - angle);
                angle = newAngle;
            }
        }
        if (oldPieceIdx != currentPieceIndex) {
            pieceChanged = true;
        } else {
            pieceChanged = false;
        }
        if (speed < 0) {
            speed = lastCorrectSpeed;
        } else {
            lastCorrectSpeed = speed;
        }
        position = 0;
        for (int i = 0; i < currentPieceIndex; i++) {
            position += track.getPieces().get(i).calculateLength();
        }
        position += currentInPieceDistance;
        currentSimulationPiece = (int)Math.floor(position / Constants.SIMULATION_PIECE_LENGTH);
        oldPieceIdx = currentPieceIndex;
        oldInPieceDistance = currentInPieceDistance;
    }

    public int getCurrentSimulationPiece() {
        return currentSimulationPiece;
    }

    public int getCurrentPieceIndex() {
        return currentPieceIndex;
    }

    public double getSpeed() {
        return speed;
    }

    public double getPosition() {
        return position;
    }

    public boolean isPieceChanged() {
        return pieceChanged;
    }
}
