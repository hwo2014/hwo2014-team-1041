package noobbot.simulation;

import java.util.ArrayList;
import java.util.List;

public class SimulationPiece {

    public static enum SimulationPieceType {
        NONE,
        CURVE_MIDDLE
    }
    private final boolean curve;
    private final double start, end, radius, angle;
    private final SimulationPieceType type;

    public SimulationPiece(double start, double end, boolean curve, double radius, SimulationPieceType type, double correctAngle) {
        this.curve = curve;
        this.start = start;
        this.end = end;
        this.radius = radius;
        this.type = type;
        this.angle = correctAngle;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SP");
        sb.append('[');
        sb.append(start + "->" + end);
        if (isCurve()) {
            sb.append(", radius=" + radius);
        }
        sb.append(']');
        return sb.toString();
    }

    public boolean isCurve() {
        return curve;
    }

    public double getRadius() {
        if (!isCurve()) {
            throw new IllegalAccessError("Only curves have radius");
        }
        return radius;
    }

    public SimulationPieceType getType() {
        return type;
    }

}
