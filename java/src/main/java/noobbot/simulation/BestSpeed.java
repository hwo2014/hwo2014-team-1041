package noobbot.simulation;

/**
 * Created by kristjanveskimae on 19/04/2014.
 * 2633
 */
public class BestSpeed {
    private double bestSpeed;
    private double simulationSpeed;

    public BestSpeed(double bestSpeed) {
        this.bestSpeed = bestSpeed;
        this.simulationSpeed = bestSpeed;
    }

    public double getSimulationSpeed() {
        return simulationSpeed;
    }

    public void setSimulationSpeed(double simulationSpeed) {
        this.simulationSpeed = simulationSpeed;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(simulationSpeed);
        return sb.toString();
    }
}
