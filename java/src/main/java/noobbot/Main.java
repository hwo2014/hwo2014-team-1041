package noobbot;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import noobbot.ai.AI;
import noobbot.io.*;
import noobbot.model.Track;
import noobbot.regression.ThrottleRealdata;
import noobbot.simulation.BestSpeed;
import noobbot.simulation.Simulation;
import noobbot.simulation.SimulationPiece;
import noobbot.simulation.State;

public class Main {

    final Gson gson = new Gson();
    private PrintWriter writer;
    private State state;
    private AI ai;
    private List<ThrottleRealdata> throttleRealdatas = new ArrayList<>();
    private double prevSpeed, prevThrottle = -1;

    public static void main(String... args) throws IOException {
        // TODO use program arguments
        String host = "hakkinen.helloworldopen.com";
        int port = 8091;
        String botName = "Dragons";
        String botKey = "8WyuNo2NGdrTZA";

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                state.update(msgFromServer.data);
                int currentSimulationPiece = state.getCurrentSimulationPiece();
                double nextSpeed = ai.getNextPiece(currentSimulationPiece).getSimulationSpeed();
                double throttle = AI.calculateThrottle(state.getSpeed(), nextSpeed);
                // System.out.println("position=" + state.getPosition() + ", speed=" + state.getSpeed() + ", nextSpeed=" + nextSpeed + ", throttle=" + throttle);

                // state.getSpeed() + nextSpeed + throttle;
                if (state.isPieceChanged()) {
                    String turn = null;
                    if ((turn = AI.laneChange(state.getCurrentPieceIndex())) != null) {
                        send(new Switch(turn));
                        System.out.println("SWITCHING TO LANE " + turn);
                    } else {
                        send(new Throttle(throttle));
                    }
                    prevThrottle = -1;
                } else {
                    send(new Throttle(throttle));
                    if (prevThrottle != -1) {
                        throttleRealdatas.add(new ThrottleRealdata(prevSpeed, state.getSpeed(), prevThrottle));
                    }
                    prevSpeed = state.getSpeed();
                    prevThrottle = throttle;
                }
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                Track track = new Track();
                track.handleGameInit(msgFromServer.data);
                // System.out.println(track);
                Simulation simulation = Simulation.createSimulation(track);
                // System.out.println(simulation);
                state = new State(track);
                ai = new AI(simulation);
                //System.out.println(ai.getBestSpeedMap());
                /*
                List<SimulationPiece> simulationPieces = simulation.getPieces();
                for (int i = 0; i < simulationPieces.size(); i++) {
                    SimulationPiece piece = simulationPieces.get(i);
                    BestSpeed bestSpeed = ai.getBestSpeedMap().get(i);
                    System.out.println("piece=" + piece + ", bestSpeed=" + bestSpeed);
                }
                */
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                saveThrottleValues(throttleRealdatas, "throttlefunction.txt");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                System.out.println(msgFromServer.msgType);
                send(new Ping());
            }
        }
    }

    public static void saveThrottleValues(List<ThrottleRealdata> throttles, String filename) {
        try {
        File file = new File(filename);
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        for (ThrottleRealdata throttleRealdata : throttles) {
            bw.write(throttleRealdata.getCsv());
            bw.newLine();
        }
        bw.flush();
        bw.close();
        } catch (Exception e) {
            System.out.println("Unable to save throttle function to file");
            e.printStackTrace();
        }

    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}