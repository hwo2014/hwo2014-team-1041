package noobbot.regression;

public class ThrottleRealdata {

    public final double speed, nextSpeed, throttle;

    public ThrottleRealdata(double speed, double nextSpeed, double throttle) {
        this.speed = speed;
        this.nextSpeed = nextSpeed;
        this.throttle = throttle;
    }

    public String getCsv() {
        StringBuilder sb = new StringBuilder();
        sb.append(speed).append(',').append(nextSpeed).append(',').append(throttle);
        return sb.toString();
    }

}
