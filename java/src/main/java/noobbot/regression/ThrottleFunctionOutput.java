package noobbot.regression;

import noobbot.Main;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kristjanveskimae on 21/04/2014.
 */
public class ThrottleFunctionOutput {

    public static void main(String[] args) throws Exception {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("throttlefunction.txt")));
        String line = null;
        List<ThrottleRealdata> realdataList = new ArrayList<>();
        while ((line = bufferedReader.readLine()) != null) {
            // System.out.println(line);
            String[] splits = line.split(",");
            if (splits.length == 3) {
                double initialSpeed = Double.parseDouble(splits[0]);
                double finalSpeed = Double.parseDouble(splits[1]);
                double throttle = Double.parseDouble(splits[2]);
                ThrottleRealdata realdata = new ThrottleRealdata(initialSpeed, finalSpeed, throttle);
                realdataList.add(realdata);
            }
        }
        List<ThrottleRealdata> toBeRemoved = new ArrayList<>();
        for (ThrottleRealdata realdata : realdataList) {
            if (realdata.nextSpeed <= 0.01) {
                toBeRemoved.add(realdata);
            }
        }
        realdataList.removeAll(toBeRemoved);
        Map<Integer, List<ThrottleRealdata>> organized = new HashMap<>();
        for (ThrottleRealdata realdata : realdataList) {
            int roundedSpeed = (int) Math.round(realdata.speed);
            List<ThrottleRealdata> throttles = organized.get(roundedSpeed);
            if (throttles == null) {
                throttles = new ArrayList<>();
                organized.put(roundedSpeed, throttles);
            }
            throttles.add(realdata);
        }

        try {
            File file = new File("throttles2.txt");
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            int max = 0;
            for (List<ThrottleRealdata> data2 : organized.values()) {
                if (data2.size() > max) {
                    max = data2.size();
                }
            }
            boolean isContinue = true;
            int lineNo = 0;
            for (Integer key : organized.keySet()) {
                bw.write(key + ",,,");
            }
            bw.newLine();
            while (isContinue) {
                isContinue = false;
                StringBuilder sb = new StringBuilder();
                for (Integer key : organized.keySet()) {
                    List<ThrottleRealdata> data = organized.get(key);
                    if (data.size() > lineNo) {
                        ThrottleRealdata cur = data.get(lineNo);
                        sb.append(cur.speed).append(',').append(cur.nextSpeed).append(',').append(cur.throttle);
                        isContinue = true;
                    } else {
                        sb.append(',').append(',');
                    }
                    sb.append(',');
                }
                bw.write(sb.toString());
                bw.newLine();
                lineNo++;
            }
            /*
            for (Integer key : organized.keySet()) {
                List<ThrottleRealdata> data = organized.get(key);
                for (ThrottleRealdata datapiece : data) {
                    bw.write(datapiece.);
                }
                bw.newLine();
            }
            */
            bw.flush();
            bw.close();
        } catch (Exception e) {
            System.out.println("Unable to save throttle function to file");
            e.printStackTrace();
        }
    }

}
