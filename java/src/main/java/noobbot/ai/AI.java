package noobbot.ai;

import noobbot.simulation.BestSpeed;
import noobbot.simulation.Constants;
import noobbot.simulation.Simulation;
import noobbot.simulation.SimulationPiece;

import java.util.*;

public class AI {

    private Map<Integer, BestSpeed> bestSpeedMap = new HashMap<>();

    public AI(Simulation simulation) {
        initializeBestSpeeds(simulation.getPieces());
        correctBestSpeedsForSlowdownsBeforeCurves(simulation.getPieces());
    }

    public Map<Integer, BestSpeed> getBestSpeedMap() {
        return bestSpeedMap;
    }

    private void initializeBestSpeeds(List<SimulationPiece> pieces) {
        for (int i = 0; i < pieces.size(); i++) {
            double bestSpeedValue;
            SimulationPiece piece = pieces.get(i);
            if (piece.isCurve()) {
                if (piece.getType().equals(SimulationPiece.SimulationPieceType.CURVE_MIDDLE)) {
                    bestSpeedValue = Math.sqrt(Constants.SMALL_NORMAL_FORCE * (piece.getRadius() - Constants.LANE_WIDTH));
                } else {
                    bestSpeedValue = Math.sqrt(Constants.NORMAL_FORCE * (piece.getRadius() - Constants.LANE_WIDTH));
                }
            } else {
                bestSpeedValue = Constants.MAX_SPEED;
            }
            BestSpeed bestSpeed = new BestSpeed(bestSpeedValue);
            bestSpeedMap.put(i, bestSpeed);
        }
    }

    private void correctBestSpeedsForSlowdownsBeforeCurves(List<SimulationPiece> pieces) {
        Iterator<SimulationPiece> iterator = pieces.iterator();
        boolean inCurve = false;
        while (iterator.hasNext()) {
            SimulationPiece piece = iterator.next();
            if (piece.isCurve()) {
                if (!inCurve) {
                    inCurve = true;
                    int idx = pieces.indexOf(piece);
                    double optimalSpeed = bestSpeedMap.get(idx).getSimulationSpeed();
                    int cur = idx - 1;
                    while (cur > 0 && !pieces.get(cur).isCurve() && optimalSpeed < Constants.MAX_SPEED) {
                        optimalSpeed = optimalSpeed + Constants.SIMULATION_PIECE_LENGTH * Constants.MAX_ACCELERATION / optimalSpeed;
                        if (optimalSpeed > Constants.MAX_SPEED) {
                            optimalSpeed = Constants.MAX_SPEED;
                        }
                        bestSpeedMap.get(cur).setSimulationSpeed(optimalSpeed);
                        cur--;
                    }
                }
            } else {
                inCurve = false;
            }
        }
    }

    public BestSpeed getNextPiece(int idx) {
        if (idx < bestSpeedMap.size() - 1) {
            return bestSpeedMap.get(idx + 1);
        } else {
            return bestSpeedMap.get(0);
        }
    }

    public static double calculateThrottle(double speed, double nextSpeed) {
        double ret;
        if (nextSpeed >= Constants.MAX_SPEED) {
            ret = 1.0;
        } else {
            double acceleration = (nextSpeed - speed) * speed / Constants.SIMULATION_PIECE_LENGTH;
            ret = Constants.THROTTLE_FROM_ACCELERATION_MULTIPLIER * (acceleration + Constants.THROTTLE_FROM_ACCELERATION_ADD);
        }
        if (ret > 1.0) {
            ret = 1.0;
        } else if (ret < 0.0) {
            ret = 0.0;
        }
        return ret;
    }

    private static List<Integer>
            leftSwitches = new ArrayList<>(Arrays.asList(7
            // , 17
    )),
            rightSwitches = new ArrayList<>(Arrays.asList(2
               //     , 12
                    ,17
            ));

    public static String laneChange(int newPieceIndex) {
        if (rightSwitches.contains(newPieceIndex)) {
            return "Right";
        } else if (leftSwitches.contains(newPieceIndex)) {
            return "Left";
        }
        return null;
    }

}
